package com.example.maven.dao;

import com.example.maven.commons.dao.GenericDao;
import com.example.maven.domain.TestEntity;


public interface TestDao extends GenericDao<TestEntity, Long> {

}
