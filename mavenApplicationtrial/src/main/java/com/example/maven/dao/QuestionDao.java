package com.example.maven.dao;

import com.example.maven.commons.dao.GenericDao;
import com.example.maven.domain.QuestionEntity;

public interface QuestionDao extends GenericDao<QuestionEntity, Long> {

}
