package com.example.maven.dao;

import com.example.maven.commons.dao.GenericDao;
import com.example.maven.domain.UserEntity;

/**
 * Data access object to work with entity operations
 * @author Przemas
 *
 */

public interface UserDao extends GenericDao<UserEntity, Long> {

    /**
     * Queries database for user name availability
     * 
     * @param userName
     * @return true if available
     */
    boolean checkAvailable(String userName);
    
    /**
     * Queries user by username
     * 
     * @param userName
     * @return User entity
     */
    UserEntity loadUserByUserName(String userName);
}