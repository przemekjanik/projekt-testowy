package com.example.maven.dao;

import com.example.maven.commons.dao.GenericJpaDao;
import com.example.maven.domain.QuestionEntity;

public class QuestionJpaDao extends GenericJpaDao<QuestionEntity, Long>
		implements QuestionDao {

	public QuestionJpaDao() {
		super(QuestionEntity.class);
	}

}
