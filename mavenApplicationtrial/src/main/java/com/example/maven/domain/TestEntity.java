package com.example.maven.domain;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.example.maven.commons.domain.BaseEntity;

@Entity
@Table(name = "test")
public class TestEntity extends BaseEntity {

	private Long maxPoints;
	private String name;



	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getMaxPoints() {
		return maxPoints;
	}

	public void setMaxPoints(Long maxPoints) {
		this.maxPoints = maxPoints;
	}

	

}
