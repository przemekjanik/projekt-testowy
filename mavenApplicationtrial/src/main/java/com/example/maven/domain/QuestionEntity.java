package com.example.maven.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.example.maven.commons.domain.BaseEntity;

@Entity
@Table(name = "question")
public class QuestionEntity extends BaseEntity{

	private Long testId;
	private Long type;
	private Long points;
	private String name;
	private String Answer;
	public Long getTestId() {
		return testId;
	}
	public void setTestId(Long testId) {
		this.testId = testId;
	}
	public Long getType() {
		return type;
	}
	public void setType(Long type) {
		this.type = type;
	}
	public Long getPoints() {
		return points;
	}
	public void setPoints(Long points) {
		this.points = points;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAnswer() {
		return Answer;
	}
	public void setAnswer(String answer) {
		Answer = answer;
	}
	
	
	
}
