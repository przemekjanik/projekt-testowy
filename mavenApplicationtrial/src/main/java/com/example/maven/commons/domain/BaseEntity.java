package com.example.maven.commons.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Klasa bazowa przechowujaca id do bazy dancyh
 * 
 * @author Przemas
 * 
 */

//Oznacza, ze klasa bedzie tylko superklasa oraz m�wi Hibernate aby nie torzyl tabeli tej klasy
@MappedSuperclass
public class BaseEntity implements Serializable {
        private static final long serialVersionUID = 568379222048217476L;

        @Id
        @GeneratedValue
        @Column(insertable = false, updatable = false)
        private Long id;

        public Long getId() {
                return id;
        }

        public void setId(Long id) {
                this.id = id;
        }
        
}