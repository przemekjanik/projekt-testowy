package com.example.maven.commons.dao;

import java.io.Serializable;
import java.util.List;

/**
 * Generyczny interfejs dla dost�pu do bazy danych.
 * @author Przemas
 *
 */

public interface GenericDao<T, ID extends Serializable> {
    
    T save(T entity);
    T update(T entity);
    void delete(T entity);
    T findById(ID id);
    List<T> findAll();
    void flush();
}
