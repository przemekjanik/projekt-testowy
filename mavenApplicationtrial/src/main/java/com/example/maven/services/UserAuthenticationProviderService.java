package com.example.maven.services;

import com.example.maven.domain.UserEntity;

public interface UserAuthenticationProviderService {

	
	 boolean processUserAuthentication(UserEntity user);
}
