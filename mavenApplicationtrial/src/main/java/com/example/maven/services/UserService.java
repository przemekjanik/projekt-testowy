package com.example.maven.services;



import java.util.List;

import javax.faces.event.AjaxBehaviorEvent;

import com.example.maven.domain.UserEntity;



/**
 * Service providing service methods to work with user data and entity.
 *
 * @author Arthur Vin
 */
public interface UserService {

    /**
     * Create user - persist to database
     *
     * @param userEntity
     * @return true if success
     */
    boolean createUser(UserEntity userEntity);
   
    /**
     * Check user name availability. UI ajax use.
     *
     * @param ajax event
     * @return
     */
    boolean checkAvailable(AjaxBehaviorEvent event);
   
    /**
     * Retrieves full User record from database by user name
     *
     * @param userName
     * @return UserEntity
     */
    UserEntity loadUserEntityByUsername(String userName);
    
    UserEntity update (UserEntity userEntity);
    
    void delete(UserEntity userEntity);
    
    
    List <UserEntity> listAllUsers();
}
