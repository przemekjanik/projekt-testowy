package com.example.maven.services.impl;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.RowEditEvent;

import com.example.maven.dao.TestDao;
import com.example.maven.domain.TestEntity;
import com.example.maven.domain.TestEntity;
import com.example.maven.services.TestService;
import com.example.maven.ui.table.TableOperations;
import com.example.maven.ui.utils.DataModel;

public class TestServiceImpl implements TestService, TableOperations {

	private TestDao testDao;
	private List<TestEntity> filteredTests;
	private TestEntity[] selectedTests;
	private DataModel<TestEntity> dataModel;

	public DataModel<TestEntity> getDataModel() {
		return dataModel;
	}

	public void setDataModel(DataModel<TestEntity> dataModel) {
		this.dataModel = dataModel;
	}

	public List<TestEntity> getFilteredTests() {
		return filteredTests;
	}

	public void setFilteredTests(List<TestEntity> filteredTests) {
		this.filteredTests = filteredTests;
	}

	public TestEntity[] getSelectedTests() {
		return selectedTests;
	}

	public void setSelectedTests(TestEntity[] selectedTests) {
		this.selectedTests = selectedTests;
	}

	public TestDao getTestDao() {
		return testDao;
	}

	public void setTestDao(TestDao testDao) {
		this.testDao = testDao;
	}

	public void createTest(TestEntity testEntity) {
		testDao.save(testEntity);

	}

	public List<TestEntity> listAll() {
		List<TestEntity> lista = testDao.findAll();
		if (dataModel == null) {
			dataModel = new DataModel<TestEntity>(lista);
		}

		return lista;
	}

	public void onEdit(RowEditEvent event) {
		TestEntity entity = ((TestEntity) event.getObject());

		testDao.update(entity);
		FacesMessage msg = new FacesMessage(TestEntity.class.toString(),
				entity.getName());

		FacesContext.getCurrentInstance().addMessage(null, msg);

	}

	public void onCancel(RowEditEvent event) {
		FacesMessage msg = new FacesMessage("Car Cancelled",
				((TestEntity) event.getObject()).getName());

		FacesContext.getCurrentInstance().addMessage(null, msg);

	}

	public void print(ActionEvent actionEvent) {
		System.out.println(dataModel == null ? "true" : "false");

		for (TestEntity entity : selectedTests) {
			System.err.println("usuwam " + entity.getId() + " "
					+ entity.getName());
			testDao.delete(entity);
		}

	}

}
