package com.example.maven.services;

import java.util.List;

import com.example.maven.domain.TestEntity;

public interface TestService {

	public void createTest(TestEntity testEntity);
	public List<TestEntity> listAll();
	
}
