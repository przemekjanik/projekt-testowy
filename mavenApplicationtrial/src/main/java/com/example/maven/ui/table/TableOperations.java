package com.example.maven.ui.table;

import javax.faces.event.ActionEvent;
import org.primefaces.event.RowEditEvent;

public interface TableOperations {

	public void onEdit(RowEditEvent event);

	public void onCancel(RowEditEvent event);

	public void print(ActionEvent actionEvent);

}
