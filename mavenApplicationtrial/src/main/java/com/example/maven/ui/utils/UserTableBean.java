package com.example.maven.ui.utils;

import java.io.Serializable;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.application.FacesMessage;

import org.primefaces.event.RowEditEvent;

import com.example.maven.domain.UserEntity;
import com.example.maven.services.impl.UserServiceImpl;

public class UserTableBean implements Serializable {

	private UserServiceImpl userServiceImpl;
	private List<UserEntity> filteredUsres;
	private UserEntity[] selectedUsers;
	private DataModel <UserEntity> dataModel;




	/*
	 * geti i sety
	 */

	public UserServiceImpl getUserServiceImpl() {
		return userServiceImpl;
	}

	public List<UserEntity> getFilteredUsres() {
		return filteredUsres;
	}

	public void setFilteredUsres(List<UserEntity> filteredUsres) {
		
		this.filteredUsres = filteredUsres;
	}

	public UserEntity[] getSelectedUsers() {
		System.err.println("getSelectedUsers()");
		return selectedUsers;
	}

	public void setSelectedUsers(UserEntity[] selectedUsers) {

		System.err.println("setSelectedUsers()");
		for (UserEntity user : selectedUsers) {
			System.err.println(user.getId() + " " + user.getFirstName() + " "
					+ user.getLastName());
		}
		this.selectedUsers = selectedUsers;
	}

	public DataModel <UserEntity> getDataModel() {
		System.err.println("getDataModel()");
		return dataModel;
	}

	public void setDataModel(DataModel <UserEntity> dataModel) {
		System.err.println("setDataModel()");
		this.dataModel = dataModel;
	}

	

	public void setUserServiceImpl(UserServiceImpl userServiceImpl) {
		this.userServiceImpl = userServiceImpl;
		dataModel = new DataModel<UserEntity>(userServiceImpl.getList());
		
	}

	/*
	 * Komunikaty edycji wierszy
	 */
	public void onEdit(RowEditEvent event) {
		UserEntity userEntity = ((UserEntity) event.getObject());

		userServiceImpl.update(userEntity);
		FacesMessage msg = new FacesMessage("Car Edited",
				userEntity.getFirstName());

		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public void onCancel(RowEditEvent event) {
		FacesMessage msg = new FacesMessage("Car Cancelled",
				((UserEntity) event.getObject()).getLastName());

		FacesContext.getCurrentInstance().addMessage(null, msg);

	}

	public void print(ActionEvent actionEvent) {
		
		System.out.println(dataModel==null?"true":"false");
		
		for (UserEntity user : selectedUsers) {
			System.err.println("wyswietlam " + user.getId() + " "
					+ user.getFirstName() + " " + user.getLastName());
		}
	}
}
