package com.example.maven.ui.utils;


import java.util.List;  

import javax.faces.model.ListDataModel;  

import org.primefaces.model.SelectableDataModel;  

import com.example.maven.commons.domain.BaseEntity;


public class DataModel<T> extends ListDataModel<T> implements SelectableDataModel<T> {    
	  
 
	  
    
  
    public DataModel(List<T> data) {  
    	
        super(data);
        System.err.println("utworzenie data model list");
    }  
      
    
    public T getRowData(String rowKey) {  
        //In a real app, a more efficient way like a query by rowKey should be implemented to deal with huge data  
    	System.err.println("getrowdata();");
        List<T> entities = (List<T>) getWrappedData();  
        
        for(T entity : entities) {  
            if(((BaseEntity)entity).getId().equals(rowKey))  
            	System.err.println("getrowdata(); Zwraca znaleziony wiersz");
                return entity;  
        }  
        System.err.println("getrowdata(); zwraca null");
        return null;  
    }  
  
    
    public Object getRowKey(T entity) {  
    	System.err.println("getrowkey();");
        return ((BaseEntity)entity).getId();  
    }  
} 
   